<?php

require_once 'FaceFinderInterface.php';
require_once 'DBConnection.php';
require_once 'Face/Service.php';
require_once 'SimilarTop/Service.php';

class FaceFinder implements FaceFinderInterface
{
    const MAX_FACES_COUNT = 10000;
    const TOP_SIZE = 6;

    private $dbc;
    private $inited = false;

    private $faceService;
    private $similarTopService;

    public function __construct()
    {
        $this->faceService = new Face\Service();
        $this->similarTopService = new SimilarTop\Service();
        $this->initDb();
    }

    private function getDbc(): \PDO
    {
        if ($this->dbc == null) {
            $this->dbc = DBConnection::getInstance()->getDbc();
        }

        return $this->dbc;
    }

    /**
     * Finds 5 most similar faces in DB.
     * If the specified face is new (id=0),
     * then it will be saved to DB.
     *
     * @param FaceInterface $face Face to find and save (if id=0)
     * @return FaceInterface[] List of 5 most similar faces,
     * including the searched one
     */
    public function resolve(FaceInterface $face): array
    {
        if ($face->getId() === 0) {
            $this->addFace($face);
        }
        return array_slice($this->find($face), 0, self::TOP_SIZE - 1);
    }

    /**
     * Removes all faces in DB and (!) reset faces id sequence
     */
    public function flush(): void
    {
        $this->faceService->dropTable();
        $this->similarTopService->dropTable();
        $this->inited = false;
        $this->initDb();
    }

    /**
     * @throws Exception
     */
    private function initDb()
    {
        if ($this->inited) {
            return;
        }

        $this->getDbc()->beginTransaction();
        $this->createDB();
        $this->faceService->createTable();
        $this->similarTopService->createTable();
        $this->getDbc()->commit();
        $this->inited = true;
    }

    private function createDB()
    {
        $this->getDbc()->query('CREATE DATABASE `face_finder` IF NOT EXISTS');
        $this->getDbc()->query('USE `face_finder`');
    }

    /**
     * @throws Exception
     */
    public function addFace(FaceInterface $newFace)
    {
        $this->getDbc()->beginTransaction();
        $this->faceService->insertFace($newFace);
        $updateTops = [
            $newFace,
        ];
        $removeFromTop = [];
        $addToTop = [];
        $updateFace = [];

        $allFaces = $this->faceService->getAllFaces();
        if ($newFace->getId() > self::MAX_FACES_COUNT) {
            $oldestFaceId = $newFace->getId() - self::MAX_FACES_COUNT;

            $tops = $this->similarTopService->selectTopHeadsBySimilars([$oldestFaceId]);
            foreach ($tops as $faceId) {
                if (!isset($allFaces[$faceId])) {
                    continue;
                }
                $updateTops[] = $allFaces[$faceId];
            }

            $this->faceService->removeOld($oldestFaceId);
            unset($allFaces[$oldestFaceId]);
        }

        $affectedFaces = $this->faceService->getPossibleAffectedTopHeads($newFace);
        $affectedTops = $this->getTops($affectedFaces);

        foreach ($affectedTops as $faceId => $top) {
            if ($faceId === $newFace->getId()) {
                continue;
            }

            if (!isset($affectedFaces[$faceId])) {
                continue;
            }

            $top[] = $newFace;
            if (count($top) < self::TOP_SIZE) {
                $addToTop[] = [$faceId, $newFace->getId()];
                continue;
            }
            $this->sort($top, $affectedFaces[$faceId]);
            if (count($top) > self::TOP_SIZE) {
                // получить расстояние до самой дальней точки
                $farFace = end($top);
                if ($farFace === $newFace) {
                    continue;
                }
                $removeFromTop[] = [$faceId, $farFace->getId()];
            }
            $addToTop[] = [$faceId, $newFace->getId()];

            $diff = $this->getDiff($top[self::TOP_SIZE - 1], $affectedFaces[$faceId]);
            $updateFace[$faceId] = [$affectedFaces[$faceId], $diff];
        }

        $allFaces = array_values($allFaces);
        foreach ($updateTops as $face) {
            $this->sort($allFaces, $face);
            $top = [];
            for ($i = 0; $i <= self::TOP_SIZE; ++$i) {
                if (!isset($allFaces[$i])) {
                    continue;
                }
                $top[$allFaces[$i]->getId()] = $allFaces[$i];
            }
            if (isset($top[$face->getId()])) {
                unset($top[$face->getId()]);
            }
            $top = [$face->getId() => $face] + $top;
            $top = array_slice($top, 0, self::TOP_SIZE);
            foreach ($top as $item) {
                $addToTop[] = [$face->getId(), $item->getId()];
            }
            if (count($top) < self::TOP_SIZE) {
                $updateFace[$face->getId()] = [$face];
            } else {
                $diff = $this->getDiff($top[self::TOP_SIZE - 1], $face);
                $updateFace[$face->getId()] = [$face, $diff];
            }
        }

        $this->faceService->updateMaxMinDiff($updateFace);
        $this->similarTopService->removeFromTop($removeFromTop);
        $this->similarTopService->addToTop($addToTop);

        $this->getDbc()->commit();
    }

    private function getDiff(FaceInterface $faceOne, FaceInterface $faceTwo): float
    {
        return sqrt(
            ($faceOne->getRace() - $faceTwo->getRace()) ** 2 +
            ($faceOne->getEmotion() - $faceTwo->getEmotion()) ** 2 +
            ($faceOne->getOldness() - $faceTwo->getOldness()) ** 2
        );
    }

    /**
     * @param FaceInterface[] $faces
     * @return array
     */
    public function getTops(array $faces): array
    {
        $indexedFaces = [];
        foreach ($faces as $face) {
            $indexedFaces[$face->getId()] = $face;
        }

        if (!$indexedFaces) {
            return [];
        }
        $similars = $this->similarTopService->getSimilarsByIds(array_keys($indexedFaces));

        if (!$similars) {
            return [];
        }

        $faces = $this->faceService->getFacesByIds(array_values(array_unique(array_column($similars, 'similar'))));
        $result = [];
        foreach ($similars as $row) {
            if (!isset($faces[$row['similar']])) {
                continue;
            }
            $result[$row['id']][] = $faces[$row['similar']];
        }

        foreach ($result as $faceId => &$top) {
            if (!isset($indexedFaces[$faceId])) {
                continue;
            }
            $this->sort($top, $indexedFaces[$faceId]);
        }
        return $result;
    }

    private function sort(&$facesInTop, $face)
    {
        usort($facesInTop, function ($v1, $v2) use ($face) {
            if ($v1->getId() === $face->getId()) {
                return -1;
            }
            $diff1 = $this->getDiff($v1, $face);
            $diff2 = $this->getDiff($v2, $face);
            if ($diff1 === $diff2) {
                return 0;
            }

            return $diff1 > $diff2 ? 1 : -1;
        });
    }

    /**
     * @throws Exception
     */
    public function find(FaceInterface $face): array
    {
        $tops = $this->getTops([$face]);
        return $tops ? current($tops) : [];
    }
}
