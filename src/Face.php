<?php

require_once 'FaceInterface.php';

class Face implements FaceInterface
{
    private $id = 0;
    private $race;
    private $emotion;
    private $oldness;

    public function __construct(int $race, int $emotion, int $oldness, int $id = 0)
    {
        $this->setId($id);
        $this->race    = (int) min(abs($race), 100);
        $this->emotion = (int) min(abs($emotion), 1000);
        $this->oldness = (int) min(abs($oldness), 1000);
    }

    /**
     * Returns face id or 0, if face is new
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = abs($id);
    }

    /**
     * Returns race parameter: from 0 to 100.
     */
    public function getRace(): int
    {
        return $this->race;
    }

    /**
     * Returns face emotion level: from 0 to 1000.
     */
    public function getEmotion(): int
    {
        return $this->emotion;
    }

    /**
     * Returns face oldness level: from 0 to 1000.
     */
    public function getOldness(): int
    {
        return $this->oldness;
    }
}
