<?php
namespace Face;

require_once 'src/FaceInterface.php';
require_once 'src/Face.php';
require_once 'src/FaceFinderInterface.php';
require_once 'src/DBConnection.php';

class Service
{
    private $dbc;
    private function getDbc(): \PDO
    {
        if ($this->dbc == null) {
            $this->dbc = \DBConnection::getInstance()->getDbc();
        }

        return $this->dbc;
    }

    public function createTable()
    {
        $this->getDbc()->query(
            'CREATE TABLE IF NOT EXISTS `face_finder`.`face` ('
            . '    `id`      INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,'
            . '    `race`    SMALLINT UNSIGNED NOT NULL DEFAULT \'0\','
            . '    `emotion` SMALLINT UNSIGNED NOT NULL DEFAULT \'0\','
            . '    `oldness` SMALLINT UNSIGNED NOT NULL DEFAULT \'0\','
            . '    `top_min_race` SMALLINT UNSIGNED NOT NULL DEFAULT \'0\','
            . '    `top_max_race` SMALLINT UNSIGNED NOT NULL DEFAULT \'100\','
            . '    `top_min_emotion` SMALLINT UNSIGNED NOT NULL DEFAULT \'0\','
            . '    `top_max_emotion` SMALLINT UNSIGNED NOT NULL DEFAULT \'1000\','
            . '    `top_min_oldness` SMALLINT UNSIGNED NOT NULL DEFAULT \'0\','
            . '    `top_max_oldness` SMALLINT UNSIGNED NOT NULL DEFAULT \'1000\''
            . ') Engine=InnoDB'
        )->closeCursor();
    }


    public function dropTable()
    {
        $this->getDbc()->prepare('DROP TABLE IF EXISTS `face_finder`.`face`')->execute();
    }

    public function getPossibleAffectedTopHeads(\FaceInterface $face)
    {
        $dbc = $this->getDbc();
        // определить все топы, на кот. точка может повлять
        $stm = $dbc->prepare('
            select * from `face_finder`.`face`
            where `top_min_race` <= :race
                AND `top_max_race` >= :race
                AND `top_min_emotion` <= :emotion
                AND `top_max_emotion` >= :emotion
                AND `top_min_oldness` <= :oldness
                AND `top_max_oldness` >= :oldness
        ');
        $stm->execute([
            'race' => $face->getRace(),
            'emotion' => $face->getEmotion(),
            'oldness' => $face->getOldness(),
        ]);
        $fetchedData = $stm->fetchAll(\PDO::FETCH_ASSOC);

        $affectedFaces = [];
        foreach ($fetchedData as $row) {
            $affectedFaces[(int) $row['id']] = new \Face(
                (int) $row['race'],
                (int) $row['emotion'],
                (int) $row['oldness'],
                (int) $row['id']
            );
        }

        return $affectedFaces;
    }

    public function updateMaxMinDiff(array $updateParams)
    {
        $sql = 'UPDATE `face_finder`.`face` SET '
            . '     `top_min_race` = :topMinRace,'
            . '     `top_max_race` = :topMaxRace,'
            . '     `top_min_emotion` = :topMinEmotion,'
            . '     `top_max_emotion` = :topMaxEmotion,'
            . '     `top_min_oldness` = :topMinOldness,'
            . '     `top_max_oldness` = :topMaxOldness'
            . ' WHERE `id` = :id';

        $stm = $this->getDbc()->prepare($sql);
        foreach ($updateParams as $params) {
            $bind = [];
            $face = $params[0];
            if (array_key_exists(1, $params)) {
                $diff = $params[1];
                $bind['id'] = $face->getId(); // id
                $bind['topMinRace'] = max(0,    ceil($face->getRace() - $diff)); // top_min_race
                $bind['topMaxRace'] = min(100,  ceil($face->getRace() + $diff)); // top_max_race
                $bind['topMinEmotion'] = max(0,    ceil($face->getEmotion() - $diff)); // top_min_emotion
                $bind['topMaxEmotion'] = min(1000, ceil($face->getEmotion() + $diff)); // top_max_emotion
                $bind['topMinOldness'] = max(0,    ceil($face->getOldness() - $diff)); // top_min_oldness
                $bind['topMaxOldness'] = min(1000, ceil($face->getOldness() + $diff)); // top_max_oldness
            } else {
                $bind['id'] = $face->getId(); // id
                $bind['topMinRace'] = 0; // top_min_race
                $bind['topMaxRace'] = 100; // top_max_race
                $bind['topMinEmotion'] = 0; // top_min_emotion
                $bind['topMaxEmotion'] = 1000; // top_max_emotion
                $bind['topMinOldness'] = 0; // top_min_oldness
                $bind['topMaxOldness'] = 1000; // top_max_oldness
            }
            $stm->execute($bind);
        }
    }

    public function getAllFaces(): array
    {
        $smb = $this->getDbc()->query('
            select `id`, `race`, `emotion`, `oldness`
            from `face_finder`.`face`
        ');
        $fetched = $smb->fetchAll(\PDO::FETCH_ASSOC);
        $result = [];
        foreach ($fetched as $row) {
            $result[(int) $row['id']] = new \Face(
                (int) $row['race'],
                (int) $row['emotion'],
                (int) $row['oldness'],
                (int) $row['id']
            );
        }
        return $result;
    }

    public function getFacesByIds(array $faceIds): array
    {
        $stm = $this->getDbc()->prepare(
            'SELECT `id`, `race`, `emotion`, `oldness`
            FROM `face_finder`.`face`
            WHERE `id` IN (' . rtrim(str_repeat('?, ', count($faceIds)), ', ') . ')'
        );
        $stm->execute($faceIds);
        $fetched = $stm->fetchAll(\PDO::FETCH_ASSOC);
        $result = [];
        foreach ($fetched as $row) {
            $result[(int) $row['id']] = new \Face(
                (int) $row['race'],
                (int) $row['emotion'],
                (int) $row['oldness'],
                (int) $row['id']
            );
        }
        return $result;
    }

    public function insertFace(\FaceInterface $face)
    {
        // добавить т. в БД
        $stm = $this->getDbc()->prepare('INSERT INTO `face_finder`.`face` (race, emotion, oldness) VALUES (:race, :emotion, :oldness)');
        $stm->execute([
            'race'    => $face->getRace(),
            'emotion' => $face->getEmotion(),
            'oldness' => $face->getOldness(),
        ]);
        $face->setId($this->getDbc()->lastInsertId());
    }

    public function removeOld(int $maxId)
    {
        $stm = $this->getDbc()->prepare('DELETE FROM `face_finder`.`face` WHERE `id` < ?');
        $stm->execute([$maxId]);
    }
}