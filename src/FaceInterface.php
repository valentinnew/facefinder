<?php

interface FaceInterface
{
    public function __construct(int $race, int $emotion, int $oldness, int $id = 0);

    public function setId(int $int);

    /**
     * Returns face id or 0, if face is new
     */
    public function getId(): int;

    /**
     * Returns race parameter: from 0 to 100.
     */
    public function getRace(): int;

    /**
     * Returns face emotion level: from 0 to 1000.
     */
    public function getEmotion(): int;

    /**
     * Returns face oldness level: from 0 to 1000.
     */
    public function getOldness(): int;
}
