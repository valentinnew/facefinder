<?php
namespace SimilarTop;

require_once 'src/FaceFinderInterface.php';
require_once 'src/DBConnection.php';

class Service
{
    private $dbc;
    private function getDbc(): \PDO
    {
        if ($this->dbc == null) {
            $this->dbc = \DBConnection::getInstance()->getDbc();
        }

        return $this->dbc;
    }

    public function createTable()
    {
        $this->getDbc()->query(
            'CREATE TABLE IF NOT EXISTS `face_finder`.`similar_top` ('
            . '`id` INT UNSIGNED NOT NULL DEFAULT \'0\','
            . '`similar` INT UNSIGNED NOT NULL DEFAULT \'0\','
            . 'PRIMARY KEY `pk` (`id`, `similar`),'
            . 'INDEX `i_sim` (`similar`)'
            . ') Engine=InnoDB'
        )->closeCursor();
    }

    public function dropTable()
    {
        $this->getDbc()->prepare('DROP TABLE IF EXISTS `face_finder`.`similar_top`')->execute();
    }

    public function removeFromTop($tops)
    {
        $condition = [];
        $binds = [];
        foreach ($tops as $top) {
            $condition[] = '(`id` = ? AND `similar` = ?)';
            $binds[] = $top[0];
            $binds[] = $top[1];
        }
        $stm = $this->getDbc()->prepare(
            'DELETE FROM `face_finder`.`similar_top` WHERE ' . implode(' OR ', $condition)
        );
        $stm->execute($binds);
    }

    public function addToTop($items)
    {
        $binds = [];
        foreach ($items as $item) {
            $binds[] = $item[0];
            $binds[] = $item[1];
        }
        $stm = $this->getDbc()->prepare('INSERT INTO `face_finder`.`similar_top` (`id`, `similar`) VALUES ' . $this->prepareSql(count($items), 2));
        $stm->execute($binds);
    }

    private function prepareSql($rows, $fields)
    {
        $row = '(' . rtrim(str_repeat('?, ', $fields), ', ') . ')';
        return rtrim(str_repeat($row . ', ', $rows), ', ');
    }

    public function selectTopHeadsBySimilars($faceSimilarIds): array
    {
        $stm = $this->getDbc()->prepare('
            SELECT DISTINCT(`id`)
            FROM `face_finder`.`similar_top`
            WHERE `similar` IN (' .  rtrim(str_repeat('?, ', count($faceSimilarIds)), ', ') . ')
        ');
        $stm->execute($faceSimilarIds);
        return array_column($stm->fetchAll(\PDO::FETCH_ASSOC), 'id');
    }

    public function getSimilarsByIds($faceIds)
    {
        $stm = $this->getDbc()->prepare('
            select * from `face_finder`.`similar_top`
            where `id` in (' . implode(',', array_fill(0, count($faceIds), '?')) . ')
        ');
        $stm->execute($faceIds);
        $fetched =  $stm->fetchAll(\PDO::FETCH_ASSOC);
        $result = [];
        foreach ($fetched as $row) {
            $result[] = [
                'id' => (int) $row['id'],
                'similar' => (int) $row['similar'],
            ];
        }

        return $result;
    }
}