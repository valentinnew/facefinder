<?php

class DBConnection
{
    private static $instance;

    private $dbc;

    private function __construct()
    {
        // @todo move connection params to config
        $dsn = 'mysql:host=192.168.1.201';
        $user = 'pikabu';
        $password = '6eFSuFqWZHxb4j4M';
        $this->dbc = new PDO($dsn, $user, $password);
        $this->dbc->query('SET NAMES \'utf8\'');
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getDbc(): PDO
    {
        return $this->dbc;
    }

    private function __clone()
    {
    }
}
